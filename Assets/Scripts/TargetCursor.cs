﻿using UnityEngine;
using System.Collections;

public class TargetCursor : MonoBehaviour 
{
    public string nukeType = "SmallNuke";

    public UnityEngine.UI.Image image;

    void Awake()
    {

    }

    public Vector3 worldPosition
    {
        get
        {
            Vector3 pos = transform.position;


            return pos;
        }

    }


    public void StartDrag()
    {
        if (GameManager.instance.simulating)
            return;

        TargetManager.instance.MoveExistingCursor(this);
        image.enabled = false;

    }

    public void EndDrag()
    {
        image.enabled = true;
        TargetManager.instance.StoppedMovingExistingCursor(this);
    }


}
