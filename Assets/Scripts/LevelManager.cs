﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour 
{
    public Level[] levels;

    public Level currentLevel
    {
        get;
        private set;
    }

    int currentIndex = 0;

    public static LevelManager instance;

    void Awake()
    {
        instance = this;
    }

	// Use this for initialization
	void Start () 
    {
        currentLevel = levels[currentIndex];
        currentLevel.StartLevel();

        GameManager.instance.onSimulationSuccesful += EndCurrentLevel;
	}
	
    public void EndCurrentLevel()
    {
        currentLevel.EndLevel();
    }

    public void GoToNextLevel()
    {
        if (currentIndex + 1 <= levels.Length - 1)
            currentIndex++;
    
        currentLevel = levels[currentIndex];
        print("going to next level: " + currentLevel.name);
        currentLevel.StartLevel();
    }
}
