﻿using UnityEngine;
using System.Collections;

public class Map : MonoBehaviour 
{
    public GameObject background;
    public DestructibleObject[] destructibles;

    public int smallNukeAmount = 3;
    public int bigNukeAmount = 1;
    public int megaNukeAmount = 0;
    
    public int corruptionFromMap = 3;

    void Awake()
    {
        gameObject.SetActive(false);
    }

	public void Load()
    {
        gameObject.SetActive(true);
        background = transform.Find("Background").gameObject;

        background.SetActive(false);

        destructibles = GetComponentsInChildren<DestructibleObject>(true);

        Vector3 position = transform.position;
        position.y = 0;
        transform.position = position;

        int totalHitPoints = 0;
        foreach (DestructibleObject destructible in destructibles)
        {
            destructible.gameObject.SetActive(false);
            totalHitPoints += destructible.totalHitPoints;
           
        }

        GameManager.instance.targetsToDestroy = destructibles.Length;

        WeaponManager.instance.SetWeaponAmount(smallNukeAmount, bigNukeAmount, megaNukeAmount);
    }
}
