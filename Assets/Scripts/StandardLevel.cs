﻿using UnityEngine;
using System.Collections;

public class StandardLevel : Level 
{
    public string[] startMessages;
    public string[] endMessages;

    public override void StartLevel()
    {
        StartCoroutine(StartAnimation());
    }

    IEnumerator StartAnimation()
    {
        GlobalInputBlocker.instance.image.enabled = true;
        NotificationBox.instance.Show("Retrieving location from memory banks");

        yield return StartCoroutine(MapLoader.instance.LoadMap(map));

        yield return new WaitForSeconds(0.2f);

        NotificationBox.instance.Hide();

        yield return new WaitForSeconds(0.2f);

        yield return StartCoroutine(InfoBoxManager.instance.ShowText(startMessages));
        GlobalInputBlocker.instance.image.enabled = false;
    }

    public override void EndLevel()
    {
        StartCoroutine(EndAnimation());
    }

    IEnumerator EndAnimation()
    {
        yield return StartCoroutine(InfoBoxManager.instance.ShowText(endMessages));

        yield return StartCoroutine(MapLoader.instance.UnloadMap(map));
        GlobalInputBlocker.instance.image.enabled = false;

        LevelManager.instance.GoToNextLevel();
    }
}
