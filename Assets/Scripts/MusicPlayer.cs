﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour 
{

    public static MusicPlayer instance;
       
	// Use this for initialization
	void Start () 
    {
        if (instance != null)
            Destroy(gameObject);
        else
        {
            instance = this;
            GetComponent<AudioSource>().Play();
            DontDestroyOnLoad(gameObject);
        }
	}
}
