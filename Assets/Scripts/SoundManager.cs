﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour 
{
    [System.Serializable]
    public class Sound
    {
        public string name;
        public float volume = 0.8f;
        public List<AudioClip> clips;
    }

    public List<Sound> sounds;

    public static SoundManager instance;

    AudioSource source;

	// Use this for initialization
	void Start () 
    {
	    if(instance == null)
        {
            instance = this;
            source = GetComponent<AudioSource>();
        }
        else
        {
            Destroy(gameObject);
        }
	}
	
    public void PlaySound(string name)
    {
        Sound sound = sounds.Find(s => s.name == name);

        if (sound == null)
            Debug.LogError("no sound by that name man");
                
        AudioClip clip = sound.clips.GetRandom();

        source.PlayOneShot(clip, sound.volume);
    }

}
