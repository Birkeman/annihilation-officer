﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TargetManager : MonoBehaviour 
{

    public static TargetManager instance;
    public TargetCursor cursorPrefab;

    TargetCursor draggedCursor;

    public Sprite smallNukeSprite;
    public Sprite bigNukeSprite;
    public Sprite megaNukeSprite;


    public float bigNukeSpriteScale = 1.5f;

    public RectTransform targetCursorPanel;

    public List<TargetCursor> cursors = new List<TargetCursor>();

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GameManager.instance.onSimulationSuccesful += OnSimulationSuccesful;
    }

    bool dragValid = false;
    string draggedItem;
 
	// Update is called once per frame
	void Update () 
    {
        if (draggedCursor == null)
            return;

        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(targetCursorPanel, Input.mousePosition, Camera.main, out pos);
        draggedCursor.transform.position = targetCursorPanel.transform.TransformPoint(pos);

        if (Input.GetMouseButtonUp(0))
        {
            draggedCursor.EndDrag();
            draggedCursor = null;
        }
	}

    public void StartCursorPlacement(string nukeType)
    {
        if (WeaponManager.instance.AnyNukesLeft(nukeType) == false)
            return;

        draggedItem = name;

        TargetCursor cursor = Instantiate<TargetCursor>(cursorPrefab);

        Sprite sprite;
        if (nukeType == NukeType.small)
            sprite = smallNukeSprite;
        else if(nukeType == NukeType.big)
            sprite = bigNukeSprite;
        else
            sprite = megaNukeSprite;
        
        cursor.nukeType = nukeType;
        cursor.transform.Find("WeaponType").GetComponent<UnityEngine.UI.Image>().sprite = sprite;

        if (nukeType == NukeType.big)
        {
            Vector3 scale = cursor.transform.Find("WeaponType").transform.localScale;
            scale *= bigNukeSpriteScale;
            cursor.transform.Find("WeaponType").transform.localScale = scale;
        }
        cursor.GetComponent<RectTransform>().SetParent(targetCursorPanel, false);


        cursors.Add(cursor);

        cursor.StartDrag();

        WeaponManager.instance.NukeGrabbed(nukeType);
    }
    
    public void MoveExistingCursor(TargetCursor cursor)
    {
        draggedCursor = cursor;
    }

    public void StoppedMovingExistingCursor(TargetCursor cursor)
    {
        if (dragValid)
            return;

        WeaponManager.instance.NukeReturned(cursor.nukeType);
        cursors.Remove(cursor);
        Destroy(cursor.gameObject);
    }

    public void ValidateDrag()
    {
        dragValid = true;
    }

    public void InvalidateDrag()
    {
        dragValid = false;
    }

    void OnSimulationSuccesful()
    {
        foreach(TargetCursor cursor in cursors)
        {
            Destroy(cursor.gameObject);
        }

        cursors.Clear();
    }
}
