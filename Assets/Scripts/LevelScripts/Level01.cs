﻿using UnityEngine;
using System.Collections;

public class Level01 : Level 
{
    public GameObject[] stuffToTurnOn;
    public float startDelay = 1;
    public float minTimeBetweenTurningOn = 0.4f;
    public float maxTimeBetweenTurningOn = 0.6f;

    public float memoryFullDelay = 1f;

    public string[] messages;
    public string[] endLevelMessages;

	IEnumerator StartAnimation()
    {
        GlobalInputBlocker.instance.image.enabled = true;

        yield return new WaitForSeconds(startDelay);

        NotificationBox.instance.Show("Initialising virtual neural interface");

        yield return new WaitForSeconds(0.1f);
        
        foreach(GameObject go in stuffToTurnOn)
        {
            go.SetActive(true);
            SoundManager.instance.PlaySound("OnBlip");
            yield return new WaitForSeconds(Random.Range(minTimeBetweenTurningOn, maxTimeBetweenTurningOn));
        }

        yield return new WaitForSeconds(0.1f);
        
        NotificationBox.instance.Show("Retrieving memory sectors");

        MemoryBoxManager.instance.LoadFreshMemory();
        yield return StartCoroutine(MemoryBoxManager.instance.AnimateMemoryUnitsLoading());

        yield return new WaitForSeconds(memoryFullDelay);

        yield return new WaitForSeconds(0.1f);

        NotificationBox.instance.Show("Retrieving location from memory banks");

        yield return StartCoroutine(MapLoader.instance.LoadMap(map));

        yield return new WaitForSeconds(0.2f);

        NotificationBox.instance.Hide();

        yield return new WaitForSeconds(0.2f);

        yield return StartCoroutine(InfoBoxManager.instance.ShowText(messages));

        GlobalInputBlocker.instance.image.enabled = false;
    }

    public override void StartLevel()
    {
        foreach (GameObject ob in stuffToTurnOn)
            ob.SetActive(false);

        StartCoroutine(StartAnimation());
    }

    public override void EndLevel()
    {
        print("called twice");
        StartCoroutine(EndAnimation());
    }

    IEnumerator EndAnimation()
    {
        GlobalInputBlocker.instance.image.enabled = true;
        yield return StartCoroutine(InfoBoxManager.instance.ShowText(endLevelMessages));

        yield return StartCoroutine(MapLoader.instance.UnloadMap(map));
        GlobalInputBlocker.instance.image.enabled = false;

        LevelManager.instance.GoToNextLevel();
    }
}
