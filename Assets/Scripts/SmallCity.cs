﻿using UnityEngine;
using System.Collections;

public class SmallCity : DestructibleObject 
{
    public GameObject undamaged;
    public GameObject destroyed;

    bool damaged = false;

    public float corruptionChance = 0.1f;

    public override void TakeDamage()
    {
        if (damaged)
            return;

        undamaged.SetActive(false);
        destroyed.SetActive(true);
        damaged = true;

        if (Random.Range(0, 1) < corruptionChance)
            MemoryBoxManager.instance.CorruptSector();
        

        GameManager.instance.TargetDestroyed();
    }
    
    public override void ResetDamage()
    {
        if (!damaged)
            return;

        damaged = false;
        undamaged.SetActive(true);
        destroyed.SetActive(false);
    }
}
