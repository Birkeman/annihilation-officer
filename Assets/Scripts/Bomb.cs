﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour 
{
    public float dropSpeed = 4;

    public Vector3 target;
    public GameObject detonation;
    
	// Use this for initialization
	void Start () 
    {
        GameManager.instance.onSimulationReset += OnSimulationReset;
	}
	
    void OnDestroy()
    {
        GameManager.instance.onSimulationReset -= OnSimulationReset;
    }

    void OnSimulationReset()
    {
        Destroy(gameObject);
    }
    
	// Update is called once per frame
	void Update () 
    {
        transform.Translate(Vector3.down * dropSpeed * Time.deltaTime);

        if(transform.position.y < target.y)
        {
            target.z = detonation.transform.position.z;
            Instantiate(detonation, target, Quaternion.identity);
            Destroy(gameObject);
        }
	}
}
