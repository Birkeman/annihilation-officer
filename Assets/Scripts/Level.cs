﻿using UnityEngine;
using System.Collections;

public abstract class Level : MonoBehaviour 
{
    public Map map;
    public abstract void StartLevel();
    public abstract void EndLevel();

}
