﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
    public int targetsToDestroy = 3;

    int targetsDestroyed = 0;

    public static GameManager instance;
    
    public event System.Action onSimulationStart = delegate{};
    public event System.Action onSimulationReset = delegate{};
    public event System.Action onSimulationSuccesful = delegate { };

    public UnityEngine.UI.Text simulationButtonText;
    public GameObject weaponInteractionBlocker;
    public CanvasGroup weaponsPanel;

    public int detonationsLeft;

    public bool simulating
    {
        get;
        private set;
    }
    
    void Awake()
    {
        simulating = false;
        instance = this;
    }

    public void SimulationButtonClicked()
    {
        if (targetsDestroyed == targetsToDestroy)
            return;

        simulating = !simulating;
        SoundManager.instance.PlaySound("ButtonClicked");

        if (simulating)
        {
            StartSimulation();
        }
        else
        {
            ResetSimulation();
        }
    }

    void StartSimulation()
    {
        weaponsPanel.interactable = false;
        simulationButtonText.text = "Reset";
        weaponInteractionBlocker.SetActive(true);
        onSimulationStart();
    }

    void ResetSimulation()
    {
        weaponsPanel.interactable = true;
        simulationButtonText.text = "Simulate";

        targetsDestroyed = 0;

        weaponInteractionBlocker.SetActive(false);
        onSimulationReset();
    }

    public void TargetDestroyed()
    {
        targetsDestroyed++;

        if (targetsDestroyed == targetsToDestroy)
            SimulationSuccesful();
    }

    void SimulationSuccesful()
    {
        StartCoroutine(WaitUntilExplosionsAreGone());
    }

    IEnumerator WaitUntilExplosionsAreGone()
    {
        while (detonationsLeft > 0)
            yield return 0;

        targetsDestroyed = 0;
        simulating = false;
        weaponsPanel.interactable = true;
        simulationButtonText.text = "Simulate";
        weaponInteractionBlocker.SetActive(false);
        onSimulationSuccesful();
    }

}
