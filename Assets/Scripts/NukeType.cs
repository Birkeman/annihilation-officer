﻿using UnityEngine;
using System.Collections;

public static class NukeType 
{
    public const string small = "SmallNuke";
    public const string big = "BigNuke";
    public const string mega = "MegaNuke";

}
