﻿using UnityEngine;
using System.Collections;

public abstract class DestructibleObject : MonoBehaviour 
{
    public int totalHitPoints;

    void Start()
    {
        GameManager.instance.onSimulationReset += ResetDamage;
    }

    public abstract void TakeDamage();

    public abstract void ResetDamage();
}
