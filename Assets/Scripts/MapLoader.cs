﻿using UnityEngine;
using System.Collections;

public class MapLoader : MonoBehaviour 
{

   
    public float mapLoadingTime = 0.6f;

    public float minDelay = 0.3f;
    public float maxDelay = 0.5f;

    public static MapLoader instance;

    GameObject map;

    void Awake()
    {
       
        instance = this;
    }
    
    public IEnumerator LoadMap(Map map)
    {
        map.Load();
        map.gameObject.SetActive(true);

        int totalIncrements = map.destructibles.Length + 1;
        int currentIncrement = 0;

        map.background.SetActive(true);
        Transform backgroundTransform = map.background.transform;
        Vector3 startScale = backgroundTransform.localScale;

        for (int i = 0; i < map.corruptionFromMap; i++)
            Invoke("CorruptSector", Random.Range(0, maxDelay));

        float t = 0;
        while(t < mapLoadingTime)
        {
            backgroundTransform.localScale = startScale * t / mapLoadingTime;
            t += Time.deltaTime;
            yield return 0;
        }
        
        backgroundTransform.localScale = startScale;
        currentIncrement++;
          
        foreach(DestructibleObject destructible in map.destructibles)
        {
            yield return new WaitForSeconds(Random.Range(minDelay, maxDelay));
            destructible.gameObject.SetActive(true);
            currentIncrement++;
            MemoryBoxManager.instance.CorruptSector();
        }
    }

    void CorruptSector()
    {
        MemoryBoxManager.instance.CorruptSector();
    }

    public IEnumerator UnloadMap(Map map)
    {
        Transform mapTransform = map.transform;
        Vector3 startScale = mapTransform.localScale;

        float t = mapLoadingTime;
        while(t > 0)
        {
            mapTransform.localScale = startScale * t / mapLoadingTime;
            t -= Time.deltaTime;
            yield return 0;
        }

        map.gameObject.SetActive(false);
        
    }
}
