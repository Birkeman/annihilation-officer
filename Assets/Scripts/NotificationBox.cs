﻿using UnityEngine;
using System.Collections;

public class NotificationBox : MonoBehaviour 
{
    public GameObject panel;
    public UnityEngine.UI.Text text;

    public static NotificationBox instance;

    void Awake()
    {
        instance = this;
    }
	
	public void Show(string message)
    {
        panel.SetActive(true);
        text.text = message;
    }

    public void Hide()
    {
        panel.SetActive(false);
    }
}
