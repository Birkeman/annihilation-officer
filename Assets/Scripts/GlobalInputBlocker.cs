﻿using UnityEngine;
using System.Collections;

public class GlobalInputBlocker : MonoBehaviour 
{

    public UnityEngine.UI.Image image;

    public static GlobalInputBlocker instance;

    void Awake()
    {
        image = GetComponent<UnityEngine.UI.Image>();
        image.enabled = false;
        instance = this;
    }

}
