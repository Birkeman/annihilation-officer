﻿using UnityEngine;
using System.Collections;

public class SpeedCheat : MonoBehaviour 
{
	
#if UNITY_EDITOR
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            Time.timeScale = 0.2f;
        if (Input.GetKeyDown(KeyCode.Alpha2))
            Time.timeScale = 0.5f;
        if (Input.GetKeyDown(KeyCode.Alpha3))
            Time.timeScale = 1f;
        if (Input.GetKeyDown(KeyCode.Alpha4))
            Time.timeScale = 10;
        if (Input.GetKeyDown(KeyCode.Alpha5))
            Time.timeScale = 50;
        
	}
#endif

}
