﻿using UnityEngine;
using System.Collections;

public class GlitchTransformDistortion : Glitch 
{
    public float stayCorrectMinTime = 2;
    public float stayCorrectMaxTime = 10;

    public float stayIncorrectMinTime = 0.7f;
    public float stayIncorrectMaxTime = 1f;

    bool triggered = false;

    public float triggerValue = 0.5f;

    Vector3 startPosition;
    Vector3 startScale;
    public float minOffset = 1;
    public float maxOffset = 3;

    public float minScaleDistort = 0.7f;
    public float maxScaleDistort = 0.9f;
    
    protected override void Start()
    {
        base.Start();
        startPosition = transform.position;
        startScale = Vector3.one;
    }

    protected override void OnMemoryCorrupted()
    {
        if (MemoryBoxManager.instance.corruptionProgress >= triggerValue && !triggered)
        {
            triggered = true;

            if (gameObject.activeSelf)
                StartCoroutine(Animate());
        }
    }

    IEnumerator Animate()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(stayCorrectMinTime, stayCorrectMaxTime));

            Vector3 pos = startPosition;
            
            float xOffset = Random.Range(minOffset, maxOffset);
            float yOffset = Random.Range(minOffset, maxOffset);

            if(Random.Range(0, 1) == 1)
                xOffset = -xOffset;
            if(Random.Range(0, 1) == 1)
                yOffset = -yOffset;

            pos.x += xOffset;
            pos.y += yOffset;

            transform.position = pos;

            Vector3 scale = startScale;

            float xScale = Random.Range(minScaleDistort, maxScaleDistort);
            float yScale = Random.Range(minScaleDistort, maxScaleDistort);

            if (Random.Range(0, 1) == 1)
                xScale = -xScale;
            if (Random.Range(0, 1) == 1)
                yScale = -yScale;

           // transform.localScale = scale;

            yield return new WaitForSeconds(Random.Range(stayIncorrectMinTime, stayIncorrectMaxTime));
            transform.position = startPosition;
           // transform.localScale = startScale;

           
        }
    }
}
