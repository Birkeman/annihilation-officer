﻿using UnityEngine;
using System.Collections;

public class GlitchText : Glitch 
{
    public float stayCorrectMinTime = 2;
    public float stayCorrectMaxTime = 10;

    public float stayIncorrectMinTime = 0.7f;
    public float stayIncorrectMaxTime = 0.1f;
    public Color incorrectColor = Color.white;

    public string incorrectText = "";

    bool triggered = false;
    Color startColor;

    public float triggerValue = 0.5f;

    string correctText;

    UnityEngine.UI.Text text;

    void Awake()
    {
        text = GetComponent<UnityEngine.UI.Text>();
        correctText = text.text;
        startColor = text.color;
    }

    protected override void OnMemoryCorrupted()
    {
       if(MemoryBoxManager.instance.corruptionProgress >= triggerValue && !triggered)
       {
           triggered = true;

           if(gameObject.activeSelf)
               StartCoroutine(Animate());
       }
    }

    IEnumerator Animate()
    {
        while(true)
        {
            text.text = incorrectText;
            text.color = incorrectColor;
            yield return new WaitForSeconds(Random.Range(stayIncorrectMinTime, stayIncorrectMaxTime));
            text.text = correctText;
            text.color = startColor;

            yield return new WaitForSeconds(Random.Range(stayCorrectMinTime, stayCorrectMaxTime));
        }
    }
}
