﻿using UnityEngine;
using System.Collections;

public abstract class Glitch : MonoBehaviour 
{
	// Use this for initialization
	protected virtual void Start () 
    {
        MemoryBoxManager.instance.onMemoryCorrupted += OnMemoryCorrupted;
        OnMemoryCorrupted();
	}
	
	protected abstract void OnMemoryCorrupted();
}
