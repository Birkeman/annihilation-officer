﻿using UnityEngine;
using System.Collections;

public class EndGameGlitch : Glitch 
{
    public float time0 = 0.1f;
    public float time1 = 0.1f;
    public float time2 = 0.1f;
    public float time3 = 0.1f;

    public float[] times;

    public GameObject canvas;
    public GameObject outline;

    float volume;
    AudioSource source;

    protected override void OnMemoryCorrupted()
    {
        if (MemoryBoxManager.instance.corruptionProgress >= 1f)
            StartCoroutine(Transition());
    }

    IEnumerator Transition()
    {
        source = GetComponent<AudioSource>();
        source.Play();
        volume = source.volume;

        bool state = false;

        foreach(float time in times)
        {
            SetState(state);
            yield return new WaitForSeconds(time);
            state = !state;
        }

        Application.LoadLevel(2);
    }

    void SetState(bool state)
    {
        canvas.SetActive(state);
        outline.SetActive(state);

        if (state)
            source.volume = 0;
        else
            source.volume = volume;

        LevelManager.instance.currentLevel.map.gameObject.SetActive(state);
    }
}
