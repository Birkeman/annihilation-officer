﻿using UnityEngine;
using System.Collections;

public class GlitchImage : Glitch 
{

    public float stayCorrectMinTime = 2;
    public float stayCorrectMaxTime = 10;

    public float stayIncorrectMinTime = 0.7f;
    public float stayIncorrectMaxTime = 1f;

    bool triggered = false;

    public float triggerValue = 0.5f;

    UnityEngine.UI.Image image;

    void Awake()
    {
        image = GetComponent<UnityEngine.UI.Image>();
    }

    protected override void OnMemoryCorrupted()
    {
        if (MemoryBoxManager.instance.corruptionProgress >= triggerValue && !triggered)
        {
            triggered = true;

            if (gameObject.activeInHierarchy)
                StartCoroutine(Animate());
        }
    }

    IEnumerator Animate()
    {
        while (true)
        {
            image.enabled = false;
            yield return new WaitForSeconds(Random.Range(stayIncorrectMinTime, stayIncorrectMaxTime));
            image.enabled = true;

            yield return new WaitForSeconds(Random.Range(stayCorrectMinTime, stayCorrectMaxTime));
        }
    }
}
