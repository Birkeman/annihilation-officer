﻿using UnityEngine;
using System.Collections;

public class InfoBoxManager : MonoBehaviour 
{
    public GameObject textBox;
    public UnityEngine.UI.Text text;

    public static InfoBoxManager instance;

    string[] messages;
    public GameObject button;

    public float characterDelay = 0.01f;

    void Awake()
    {
        instance = this;
    }
    
    bool proceed = false;
    public IEnumerator ShowText(string[] messages)
    {
        this.messages = messages;
        textBox.SetActive(true);

        foreach(string message in messages)
        {
            proceed = false;
            text.text = message;
            StartCoroutine(AnimateText(message));

            while (!proceed)
                yield return 0;
        }

        textBox.SetActive(false);
    }

    IEnumerator AnimateText(string message)
    {
        button.SetActive(false);

        string shownText = "";
        text.text = shownText;

        yield return new WaitForSeconds(0.2f);

        for (int i = 0; i < message.Length + 1; i++ )
        {
            shownText = message.Substring(0, i);
            SoundManager.instance.PlaySound("TextBlip");
            text.text = shownText;

            yield return new WaitForSeconds(characterDelay);
        }


        button.SetActive(true);
    }

    public void ContinueButtonClicked()
    {
        SoundManager.instance.PlaySound("ButtonClicked");
        proceed = true;
    }
}
