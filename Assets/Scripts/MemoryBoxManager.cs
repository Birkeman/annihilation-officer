﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MemoryBoxManager : MonoBehaviour 
{
    public RectTransform memoryUnitsPanel;
    public UnityEngine.UI.Image memoryUnitPrefab;
    public float minCorruptDelay = 0.3f;
    public float maxCorruptDelay = 0.4f;

    public float badSectorChange = 0.1f;

    public Color memoryOnColor;
    public Color memoryOffColor;
    public Color changeColor = Color.white;
    
    public int width = 4;
    public int height = 12;

    public int onLoadBlipAmount = 2;

    public float timeBetweenContinousCorruption = 8;

    class Location
    {
        public int x;
        public int y;

        public Location(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

    bool[,] memoryPattern;
    public Vector2 unitDimensions;
    List<Location> locations = new List<Location>();
    public event System.Action onMemoryCorrupted = delegate { };
        
    UnityEngine.UI.Image[,] memoryImages;

    int memoryOnline = 0;
    public float minTurnOnDelay = 0.2f;
    public float maxTurnOnDelay = 0.3f;
        
    public static MemoryBoxManager instance;
    int totalMemory;

    public float corruptionProgress
    {
        get
        {
            return 1 - (float)locations.Count / totalMemory;
        }
    }

    void Awake()
    {
        instance = this;
        totalMemory = 0;
    }

    public void LoadFreshMemory()
    {
        memoryPattern = new bool[width, height];
        memoryImages = new UnityEngine.UI.Image[width, height];
       

        for(int x = 0 ; x < width ; x++)
        {
            for(int y = 0 ; y < height ; y++)
            {
                if (Random.Range(0, 1f) < badSectorChange)
                    continue;

                totalMemory++;
                memoryPattern[x, y] = true;
                locations.Add(new Location(x, y));
            }
        }
    }

    public void CorruptSector()
    {
        if (locations.Count <= 0)
            return;

        Location location = locations.GetRandom();
        locations.Remove(location);

        StartCoroutine(AnimateCorruptSector(location));
        onMemoryCorrupted();
    }

    IEnumerator AnimateCorruptSector(Location location)
    {
        UnityEngine.UI.Image image = memoryImages[location.x, location.y];
        image.color = changeColor;
        SoundManager.instance.PlaySound("MemoryOffBlip");

        yield return new WaitForSeconds( Random.Range(minCorruptDelay, maxCorruptDelay));
        image.color = memoryOffColor;
    }
    
    UnityEngine.UI.Image CreateImage(int x, int y, bool on)
    {
        Vector2 pos = new Vector2(x * unitDimensions.x, -y * unitDimensions.y);
        UnityEngine.UI.Image image = Instantiate<UnityEngine.UI.Image>(memoryUnitPrefab);
        RectTransform imageRect = image.GetComponent<RectTransform>();
        imageRect.SetParent(memoryUnitsPanel);
        imageRect.localScale = Vector3.one;
        imageRect.localPosition = new Vector3(pos.x, pos.y, 0);

        return image;
    }

    public IEnumerator AnimateMemoryUnitsLoading()
    {
        int count = locations.Count;
        List<Location> spawnedLocations = new List<Location>();

        Location[] lastLocations = new Location[onLoadBlipAmount];

        while (count > 0)
        {
            if (lastLocations[0] != null)
            {
                for (int i = 0; i < onLoadBlipAmount; i++)
                {
                    Location lastLocation = lastLocations[i];
                    memoryImages[lastLocation.x, lastLocation.y].color = memoryOnColor;
                }
            }
            
            for(int i = 0 ; i < onLoadBlipAmount ; i++)
            {
                Location location = locations.GetRandom<Location>();
                bool on = memoryPattern[location.x, location.y];
                locations.Remove(location);
                spawnedLocations.Add(location);

                memoryImages[location.x, location.y] = CreateImage(location.x, location.y, on);
                memoryImages[location.x, location.y].color = changeColor;

                SoundManager.instance.PlaySound("MemoryOnBlip");

                yield return new WaitForSeconds(Random.Range(minTurnOnDelay, maxTurnOnDelay));
                

                lastLocations[i] = location;
                count--;
                if (count == 0)
                    break;
            }
        }

        for (int i = 0; i < onLoadBlipAmount; i++)
        {
            Location lastLocation = lastLocations[i];
            memoryImages[lastLocation.x, lastLocation.y].color = memoryOnColor;
        }
        
        locations = spawnedLocations;
        StartCoroutine(ContinousCorruption());
    }
	
    IEnumerator ContinousCorruption()
    {
        while(true)
        {
            yield return new WaitForSeconds(timeBetweenContinousCorruption);
            CorruptSector();

            print("corruption: " + corruptionProgress);
        }
    }
}
