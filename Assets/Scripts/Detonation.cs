﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Detonation : MonoBehaviour 
{
    public AnimationCurve interpolation;
    public Gradient gradient;
    public float stayTime = 0.5f;

    SpriteRenderer spriteRenderer;

    public string audioId;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        StartCoroutine(Animate());

        GameManager.instance.onSimulationReset += OnSimulationReset;
        GameManager.instance.detonationsLeft++;

        SoundManager.instance.PlaySound(audioId);
    }

    void OnSimulationReset()
    {
        Destroy(gameObject);
    }

    void OnDestroy()
    {
        GameManager.instance.onSimulationReset -= OnSimulationReset;
        GameManager.instance.detonationsLeft--;
    }

    IEnumerator Animate()
    {
        Vector3 startScale = transform.localScale;

        float t = 0; 
        float lastTime = interpolation[interpolation.length - 1].time;
        

        while(t < lastTime)
        {
            float i = interpolation.Evaluate(t);

            transform.localScale = startScale * i;

            float c = t / lastTime;

            spriteRenderer.color = gradient.Evaluate(c);

            yield return 0;
            t += Time.deltaTime;
        }

        transform.localScale = startScale;
        spriteRenderer.color = gradient.Evaluate(1);
        yield return new WaitForSeconds(stayTime);

        Destroy(transform.root.gameObject);
    }
        
    void OnTriggerEnter2D(Collider2D other)
    {
        DestructibleObject destructibleObject = other.GetComponent<DestructibleObject>();

        if (destructibleObject == null)
            return;

        destructibleObject.TakeDamage();
    }

}
