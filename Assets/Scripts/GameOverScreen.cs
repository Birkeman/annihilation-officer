﻿using UnityEngine;
using System.Collections;

public class GameOverScreen : MonoBehaviour 
{
    public float startDelay = 1;
    public float partsFadeInTime = 1;
    public float fadeInPause = 0.3f;
    public float headFadeInTime = 1f;

    public string message;

    public float messageStayTime = 4f;

    public float eyeFadeUpTime = 0.4f;
    public Color eyeFadeUpColor = Color.white;
    public float planeFadeUpTime = 0.2f;
    public Color planeFadeUpColor = Color.white;
    public float planeStayTime = 0.1f;

    public UnityEngine.UI.Text textField;
    public UnityEngine.UI.Image head;
    public UnityEngine.UI.Image leftEye;
    public UnityEngine.UI.Image rightEye;
    public UnityEngine.UI.Image mouth;
    public UnityEngine.UI.Image plane;

    public float endPaneluration = 1f;
    public CanvasGroup endPanel;


	// Use this for initialization
	void Start () 
    {
        StartCoroutine(Animate());
	}
	
    IEnumerator Animate()
    {
        textField.text = "";
        Color headColor = head.color;
        head.color = Color.clear;
        endPanel.alpha = 0;
        endPanel.interactable = false;

        StartCoroutine(PartsFade());

        yield return new WaitForSeconds(fadeInPause);

        float t = 0;
        while (t < headFadeInTime)
        {
            Color col = Color.Lerp(Color.clear, headColor, t / headFadeInTime);
            head.color = col;

            t += Time.deltaTime;
            yield return 0;
        }

        yield return new WaitForSeconds(startDelay);

        textField.text = message;
        yield return new WaitForSeconds(messageStayTime);
        textField.text = "";

        Color partsStartColor = leftEye.color;
        
        t = 0;

        while (t < eyeFadeUpTime)
        {
            Color col = Color.Lerp(partsStartColor, eyeFadeUpColor, t / eyeFadeUpTime);
            leftEye.color = col;
            rightEye.color = col;

            t += Time.deltaTime;
            yield return 0;
        }

        t = 0;

        while (t < planeFadeUpTime)
        {
            Color col = Color.Lerp(Color.clear, planeFadeUpColor, t / planeFadeUpTime);
            plane.color = col;

            t += Time.deltaTime;
            yield return 0;
        }
        plane.color = planeFadeUpColor;

        yield return new WaitForSeconds(planeStayTime);

        t = 0;

        while (t < endPaneluration)
        {
            endPanel.alpha = t / endPaneluration;

            t += Time.deltaTime;
            yield return 0;
        }

        endPanel.interactable = true;
        endPanel.alpha = 1f;
    }

    IEnumerator PartsFade()
    {
        Color partsColor = leftEye.color;
        
        float t = 0;
        while (t < partsFadeInTime)
        {
            Color col = Color.Lerp(Color.clear, partsColor, t / partsFadeInTime);
            leftEye.color = col;
            rightEye.color = col;
            mouth.color = col;

            t += Time.deltaTime;
            yield return 0;
        }

    }

    public void TwitterLinkClicked()
    {
        Application.OpenURL("https://twitter.com/AlexanderBirke");
    }
}
