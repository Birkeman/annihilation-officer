﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponManager : MonoBehaviour 
{
    public static WeaponManager instance;

    int smallNukeCount;
    int bigNukeCount;
    int megaNukeCount;
    
    public float minSpawnHeight;
    public float maxSpawnHeight;

    public Bomb smallNukePrefab;
    public Bomb bigNukePrefab;
    public Bomb megaNukePrefab;

    public UnityEngine.UI.Selectable smallNukeButton;
    public UnityEngine.UI.Selectable bigNukeButton;
    public UnityEngine.UI.Selectable megaNukeButton;

    public UnityEngine.UI.Text smallNukeText;
    public UnityEngine.UI.Text bigNukeText;
    public UnityEngine.UI.Text megaNukeText;

    void OnDrawGizmos()
    {
        Gizmos.DrawLine(Vector3.up * minSpawnHeight, Vector3.up * maxSpawnHeight);
    }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GameManager.instance.onSimulationStart += OnSimulationStart;
    }

    public bool AnyNukesLeft(string nukeType)
    {
        int count;

        if (nukeType == NukeType.small)
            count = smallNukeCount;
        else if (nukeType == NukeType.big)
            count = bigNukeCount;
        else
            count = megaNukeCount;

        return count > 0;
    }

    public void SetWeaponAmount(int smallNukesStartAmount, int bigNukesStartAmount, int megaNukeStartAmount)
    {
        smallNukeCount = smallNukesStartAmount;
        bigNukeCount = bigNukesStartAmount;
        megaNukeCount = megaNukeStartAmount;

        smallNukeText.text = smallNukesStartAmount.ToString();
        bigNukeText.text = bigNukesStartAmount.ToString();
        megaNukeText.text = megaNukeStartAmount.ToString();

        if (smallNukeCount == 0)
            smallNukeButton.interactable = false;
        else
            smallNukeButton.interactable = true;

        if (bigNukeCount == 0)
            bigNukeButton.interactable = false;
        else
            bigNukeButton.interactable = true;

        if (megaNukeCount == 0)
            megaNukeButton.interactable = false;
        else
            megaNukeButton.interactable = true;
    }
    
    public void NukeGrabbed(string nukeType)
    {
        if (nukeType == NukeType.small)
        {
            UpdateSmallNukeCount(-1);
        }
        else if (nukeType == NukeType.big)
        {
            UpdateBigNukeCount(-1);
        }
        else
            UpdateMegaNukeCount(-1);

    }

    public void NukeReturned(string nukeType)
    {
        if (nukeType == NukeType.small)
        {
            UpdateSmallNukeCount(1);
        }
        else if (nukeType == NukeType.big)
        {
            UpdateBigNukeCount(1);
        }
        else
            UpdateMegaNukeCount(1);

    }

    void UpdateSmallNukeCount(int change)
    {
        smallNukeCount += change;
        smallNukeText.text = smallNukeCount.ToString();
        if (smallNukeCount == 0)
            smallNukeButton.interactable = false;
        else
            smallNukeButton.interactable = true;
    }

    void UpdateBigNukeCount(int change)
    {
        bigNukeCount += change;
        bigNukeText.text = bigNukeCount.ToString();
        if (bigNukeCount == 0)
            bigNukeButton.interactable = false;
        else
            bigNukeButton.interactable = true;
    }

    void UpdateMegaNukeCount(int change)
    {
        megaNukeCount += change;
        megaNukeText.text = megaNukeCount.ToString();
        if (megaNukeCount == 0)
            megaNukeButton.interactable = false;
        else
            megaNukeButton.interactable = true;
    }

	void OnSimulationStart()
    {
        List<TargetCursor> cursors = TargetManager.instance.cursors;

        foreach(TargetCursor cursor in cursors)
        {
            Vector3 position = cursor.worldPosition;
            position.y = Random.Range(minSpawnHeight, maxSpawnHeight);
            
            Bomb bomb;
            if(cursor.nukeType == NukeType.small)
                bomb = Instantiate<Bomb>(smallNukePrefab);
            else if(cursor.nukeType == NukeType.big)
                bomb = Instantiate<Bomb>(bigNukePrefab);
            else
                bomb = Instantiate<Bomb>(megaNukePrefab);


            position.z = bomb.transform.position.z;
            bomb.transform.position = position;
            bomb.target = cursor.worldPosition;
        }
    }
}
